# Cryzen documentation

Cryzen trading platform documentation and example code.

# Manual

## General code structure

Every algorithm in Cryzen code studio consits of the definitions of two functions `setup(context)` and `run(data)`.
We will explain their usage in the following.

### Setup function
Setup function is called at the start of the algorithm and sets up the enviroment in which the main code is executed.
It allows user to pick the data streams, and, in case of backtesting, desired initial conditions.

```python
def setup(context):
    context.attribute_1 = ...
    context.attribute_2 = ...
    ...
```
The following attributes **must** be defined both for live and backtest mode
1. `context.exchanges = {exchange_name: symbols_list}` - defines the exchanges and cryptocurrency pairs that the user wants to have access to. 
Up to two cryptocurrency pairs are supported at the moment. Example values:
    *  `exchange_name = 'binance'`
    *  `symbols_list = ['BTC/USDT']`
2. `context.accounts  = { account_name: {'id': exchange_name, 'balance': {currency1 : value, currency2: value}}}` a dictionary with keys given by account names
    and the following entries in the dictionary
    * `id` (**always**) the identifier of the exchange
    * `balance` (**backtest**) a dictionary of starting balances
    Note that the code studio will ask for keys / secrets for every account specified.
3. `context.on_error` TODO

The following attributes **must** be defined for backtest mode only
* `context.start = '2018-10-20T10:00:00 UTC+0'` start of the backtesting period
* `context.end = '2018-10-21T12:00:00 UTC+0'`  end of the backtesting period

## Run function
Run fuction executes identically in both backtester and live mode. It receives a historical or live exchange data point by point, 
and returns an **ACTION** - a command for the Cryzen trading platform to place a buy/sell order or cancel previously placed but not filled order(s);
a list of **ACTION**s to be executed successively; or an empty list (no actions).

### Data structure
```
data
├── books                   contains orderbook data for the exchanges and symbols specified in setup function
│   ├── time                timestamp of the current datapoint
│   └── exchange            exchange name    
│       ├── symbol1         symbol name
│       │   ├── asks        double list with best n asks (n<=20),  [[p1,v1], [p2,v2], ..., [pn, vn]], p1 < p2 < ... < pn
│       │   └── bids        double list with best n bids (n<=20),  [[p1,v1], [p2,v2], ..., [pn, vn]], p1 > p2 > ... > pn
│       └── symbol2         another symbol
│           └── ...         same data as for symbol1
│
├── balances                balances data
│   └── account             account name
│       ├── currency1       cryptocurrency name
│       │   ├── free        balance available for trades
│       │   ├── used        balance locked in submitted but not filled orders
│       │   └── total       free + used balances
│       └── currency2       another cryptocurrency
│           └── ...
│
└── trades                  information about all the past trades 
    └── account             account name
        ├── orderID1        order id assigned by the Cryzen platform 
        │   └── order_info  detailed information about the order, see below 
        └── orderID2        another order
            └── ...
```

## ACTION
Actions are functions returning dictionaries to be processed by Cryzen platform. 
### Buy
`buy(account, symbol, amount, price, type='limit', label='buy')`
* account - account to buy on
* symbol - symbol to buy (base/quote currency)
* amount - amount in base currency
* price - price
* type -  only 'limit' (defult value) is supported
* label - a label to be displayed on plot when hovering over the correspoinding point

Returns a dictionary with the same fields as parameter names and `'uuid'` - action ID assigned by the platform

### Sell
`sell(account, symbol, amount, price, type='limit', label='sell')`
same definition for as buy function

### Cancel
`cancel(uuid)`

cancels an order with action ID = uuid. Returns a dictionary `{'type': 'cancel','uuid': uuid}`

## PLOTTING

